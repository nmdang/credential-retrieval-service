package api

import (
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/stretchr/testify/require"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/entity"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestGrantTypeCheck(t *testing.T) {
	testCases := []struct {
		name    string
		isValid bool
		grants  *entity.Grants
	}{
		{"preauthcode without two factor", true, &entity.Grants{PreAuthorizedCode: entity.PreAuthorizedCode{
			Code:        "asjkdfasifd",
			PinRequired: false,
		}}},
		{"preauthcode with two factor", false, &entity.Grants{PreAuthorizedCode: entity.PreAuthorizedCode{
			Code:        "asjkdfasifd",
			PinRequired: true,
		}}},
		{"missing preauthcode grant type", false, &entity.Grants{AuthorizationCode: entity.AuthorizationCode{
			IssuerState: "test",
		}}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := checkGrantType(tc.grants)
			require.Equal(t, got, tc.isValid)
		})
	}
}

func TestRetrieveObjectFromCredentialOffer(t *testing.T) {
	offer := entity.Offering{
		CredentialOffer: "openid-credential-offer://credential_offer=%7B%22credential_issuer%22%3A%20%22https%3A%2F%2Fcredential-issuer.example.com%22%2C%22credentials%22%3A%20%5B%22UniversityDegree_LDP%22%5D%2C%22grants%22%3A%20%7B%22urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Apre-authorized_code%22%3A%20%7B%22pre-authorized_code%22%3A%20%22adhjhdjajkdkhjhdj%22%2C%22user_pin_required%22%3A%20false%7D%7D%7D",
		TenantId:        "test",
	}

	want := &entity.CredentialOfferObject{
		CredentialIssuer: "https://credential-issuer.example.com",
		Credentials:      []string{"UniversityDegree_LDP"},
		Grants: entity.Grants{
			AuthorizationCode: entity.AuthorizationCode{IssuerState: ""},
			PreAuthorizedCode: entity.PreAuthorizedCode{
				Code:        "adhjhdjajkdkhjhdj",
				PinRequired: false,
			},
		},
	}

	got, err := getCredentialOfferObject(offer)
	if err != nil {
		t.Errorf("unexpected error occured: %v", err)
	}

	require.True(t, cmp.Equal(got.Grants, want.Grants, cmpopts.EquateEmpty()))
}

func TestRetrieveObjectFromCredentialOfferUri(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Errorf("wrong http method! got: %s, want: %s", r.Method, http.MethodGet)
		}

		if r.URL.String() != "/credential-offer.jwt" {
			t.Errorf("call to wrong url! got: %s, want: %s", r.URL.String(), "/credential-offer.jwt")
		}

		response := `{
			"credential_issuer": "https://credential-issuer.example.com",
			"credentials": [
				"UniversityDegree_LDP"
			],
			"grants": {
				"urn:ietf:params:oauth:grant-type:pre-authorized_code": {
					"pre-authorized_code": "adhjhdjajkdkhjhdj",
					"user_pin_required": false
			}}}`

		_, err := w.Write([]byte(response))
		if err != nil {
			t.Fatalf("unexpected error while responding: %v", err)
		}
	}))
	defer server.Close()

	offer := entity.Offering{
		CredentialOfferUri: "openid-credential-offer://?credential_offer_uri=" + url.QueryEscape(server.URL) + "%2Fcredential-offer.jwt",
		TenantId:           "test",
	}

	want := &entity.CredentialOfferObject{
		CredentialIssuer: "https://credential-issuer.example.com",
		Credentials:      []string{"UniversityDegree_LDP"},
		Grants: entity.Grants{
			AuthorizationCode: entity.AuthorizationCode{IssuerState: ""},
			PreAuthorizedCode: entity.PreAuthorizedCode{
				Code:        "adhjhdjajkdkhjhdj",
				PinRequired: false,
			},
		},
	}

	got, err := getCredentialOfferObject(offer)
	if err != nil {
		t.Errorf("unexpected error occured: %v", err)
	}

	require.True(t, cmp.Equal(got.Grants, want.Grants, cmpopts.EquateEmpty()))
}
