package api

import "sync"

func Listen() {
	var wg sync.WaitGroup

	wg.Add(1)
	go startMessaging(&wg)

	wg.Wait()
}
