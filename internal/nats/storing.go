package nats

import (
	"encoding/json"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwe"
	"github.com/lestrrat-go/jwx/v2/jwk"
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/jwt"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/entity"
)

// TODO need more definition - just a simple state base on Issue #2
type CredentialData struct {
	ConnectionId string `json:"connectionId"`
	TenantId     string `json:"tenantId"`
	Credentials  string `json:"credentials"`
}

func EncryptResponse(response entity.CredentialResponseObject, pub jwk.Key) *jwe.Message {
	byteArray, err := json.Marshal(response)
	if err != nil {
		log.Error("Failed to serialized Response: ", err)
	}
	message := jwt.EncryptJweMessage(byteArray, jwa.ECDH_ES_A256KW, pub)
	return message
}

func sendCredential(response entity.CredentialResponseObject, pub jwk.Key) {
	encrypt := EncryptResponse(response, pub)

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Pub, config.CurrentCredentialRetrievalConfig.StoringTopic)
	if err != nil {
		log.Fatal(err)
	}

	event, err := cloudeventprovider.NewEvent("credential/retrieval", "json", map[string]any{"msg": encrypt})
	if err != nil {
		log.Fatal(err)
	}

	if err := client.Pub(event); err != nil {
		log.Fatalf("Failed to send %v", err)
	}
}
