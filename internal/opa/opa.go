package opa

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/entity"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/request"
)

type opaResponse struct {
	Result struct {
		AcceptCredentials bool `json:"accept_credentials"`
	} `json:"result"`
}

func createBody(tenantId string, offer entity.CredentialOfferObject) []byte {
	// Create a map to hold the JSON data
	jsonData := make(map[string]interface{})

	// Populate the map with the values from the struct
	jsonData["input"] = map[string]interface{}{
		"tenant_id":         tenantId,
		"credential_issuer": offer.CredentialIssuer,
		"credentials":       offer.Credentials,
		"grants": map[string]interface{}{
			"authorization_code": map[string]interface{}{
				"issuer_state": offer.Grants.AuthorizationCode.IssuerState,
			},
			"urn:ietf:params:oauth:grant-type:pre-authorized_code": map[string]interface{}{
				"pre-authorized_code": offer.Grants.PreAuthorizedCode.Code,
				"user_pin_required":   offer.Grants.PreAuthorizedCode.PinRequired,
			},
		},
	}

	// Convert the map to JSON
	jsonBytes, err := json.Marshal(jsonData)
	if err != nil {
		log.Error(err)
		return nil
	}

	return jsonBytes
}

func AcceptCredentials(opaUrl string, tenantId string, offer entity.CredentialOfferObject) (bool, error) {
	data := createBody(tenantId, offer)

	body, err := request.Post(opaUrl, data)
	if err != nil {
		log.Errorf("error while post request: %v", err)
	}

	// Unmarshal OPA result response
	var result opaResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return false, err
	}

	return result.Result.AcceptCredentials, nil
}
