package entity

type Offering struct {
	CredentialOffer    string `json:"credential_offer, omitempty"`
	CredentialOfferUri string `json:"credential_offer_uri, omitempty"`
	TenantId           string `json:"tenantId"`
}