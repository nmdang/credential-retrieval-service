package config

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
)

type credentialRetrievalConfig struct {
	LogLevel      string `mapstructure:"logLevel"`
	isDev         bool   `mapstructure:"isDev"`
	OfferingTopic string `mapstructure:"offeringTopic"`
	StoringTopic  string `mapstructure:"storingTopic"`
	OPAUrl        string `mapstructure:"OPAUrl"`
}

var CurrentCredentialRetrievalConfig credentialRetrievalConfig

func LoadConfig() error {
	err := core.LoadConfig("CREDENTIALRETRIEVAL", &CurrentCredentialRetrievalConfig, getDefaults())
	if err != nil {
		return err
	}
	return nil
}

func getDefaults() map[string]any {
	return map[string]any{
		"logLevel":      "info",
		"isDev":         false,
		"offeringTopic": "offering",
		"storingTopic":  "storing",
		"OPAUrl":        "localhost:8181/v1/data/credential-retrieval-service/demo",
	}
}
